<?php
define('INTERFACES', __DIR__ . '/interfaces/');
define('ENTITIES', __DIR__ . '/entities/');

define('BASE_HEAL_POINTS', 100);
define('BASE_STRENGTH', 10);
define('BASE_INTELLECT', 10);
define('BASE_AGILITY', 10);
define('BASE_PHYSICAL_DEFENSE', 5);
define('BASE_MAGIC_DEFENSE', 2);
define('EXP_ADVANCED', 20);

require "./loader.php";
