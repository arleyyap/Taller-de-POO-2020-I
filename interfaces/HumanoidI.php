<?php

namespace interfaces;

interface HumanoidI
{
    /**
     * @return Array
     */
    public function getStats(): array;
}
