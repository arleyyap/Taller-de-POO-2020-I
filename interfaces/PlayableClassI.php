<?php

namespace interfaces;

/**
 *
 * @author alexa
 */
interface PlayableClassI
{
    public function getTypes(): array;
    public function getWeapon(): array;
}
