<?php

namespace entities\Skills;

class Type extends SubType
{

    private $id;
    private $name;
    private $description;
    private $subtype;
    private $damagePercentage;

    public function __construct($id, $name, $description, $subtype)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->subtype = $subtype;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of subtype
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * Set the value of subtype
     *
     * @return  self
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;

        return $this;
    }
}
