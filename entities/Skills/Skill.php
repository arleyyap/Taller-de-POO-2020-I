<?php

namespace entities\Skills;

class Skill extends Type
{

    private $name;
    private $description;
    private $type;
    private $damagePercentage;

    public function __construct($name, $description, $type, int $damagePercentage)
    {
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;
        $this->damagePercentage = $damagePercentage;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDamagePercentage()
    {
        return $this->damagePercentage;
    }

    /**
     * @param mixed $damagePercentage
     */
    public function setDamagePercentage($damagePercentage): void
    {
        $this->damagePercentage = $damagePercentage;
    }
}
