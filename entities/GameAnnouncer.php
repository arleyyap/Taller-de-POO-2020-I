<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace entities;

use entities\Skills\Skill;

/**
 * Description of GameAnnouncer
 *
 * @author pabhoz
 */
class GameAnnouncer
{

    public static function presentCharacter(\entities\Character $character)
    {
        echo "*******************************************************" . "</br>";
        echo $character->getName() . "se ha unido al <a style=\"color:#00bd4b\";>mundo:</a> </br>";
        echo $character->getName() . " es un " . $character->getRace()::getRaceName() . "</br>";
        echo "Las estadísticas de " . $character->getName() . " son:</br></br>";
        echo "Heal Points Max: " . $character->getMaxHealtPoints() . "</br>";
        echo "Strength: " . $character->getStr() . "</br>";
        echo "Intellect: " . $character->getIntl() . "</br>";
        echo "Agility: " . $character->getAgi() . "</br>";
        echo "Physical Defense: " . $character->getPDef() . "</br>";
        echo "Magic Defense: " . $character->getMDef() . "</br></br>";
        echo "<a style=\"color:#FF0000\">Habilidad:</a> " . $character->getSkills()[0]->getName() . "</br>";
        echo "<a style=\"color:#FF0000\">Tipo:</a> " . $character->getSkills()[0]->getType()->getName() . "</br>";
        echo "<a style=\"color:#FF0000\">Subtipo:</a> " . $character->getSkills()[0]->getType()->getSubtype()->getName() . "</br></br>";
    }

    public static function progressCharacter(\entities\Character $character)
    {
        $isAlive = $character->getAlive() == TRUE ? "<a style=\"color:#00bd4b\";>vivo</a>" : "<a style=\"color:#FF0000\">Muerto</a>";
        echo "*******************************************************" . "</br>";
        echo "Estado del personaje: " . $isAlive . "</br></br>";
        echo $character->getName() . " es un " . $character->getRace()::getRaceName() . "</br>";
        echo "Las estadísticas de " . $character->getName() . " son:</br></br>";
        echo "Health Points: " . $character->getHealtPoints() . "</br>";
        echo "Strength: " . $character->getStr() . "</br>";
        echo "Intellect: " . $character->getIntl() . "</br>";
        echo "Agility: " . $character->getAgi() . "</br>";
        echo "Physical Defense: " . $character->getPDef() . "</br>";
        echo "Magic Defense: " . $character->getMDef() . "</br></br>";
    }
}
