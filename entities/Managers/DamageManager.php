<?php

namespace entities\Managers;

/**
 * 
 *• die: Recibe como parámetro el personaje a ser asesinado.
 *• revive: Recibe como parámetro el personaje a ser revivido, al revivir el personaje
 *regresa con 10% de su salud máxima a menos que se reviva con una skill de
 *revivir, en ese caso aplican las normas del skill.
 */

namespace entities\Managers;

class DamageManager
{

  //die: Recibe como parámetro el personaje a ser asesinado.
  public static function die(\entities\Character $character)
  {
    echo $character->getName() . " ha muerto" . "</br>";
    $character->setAlive(FALSE);
    return $character;
  }

  /**
   * revive: Recibe como parámetro el personaje a ser revivido, al revivir el personaje
   *regresa con 10% de su salud máxima a menos que se reviva con una skill de
   *revivir, en ese caso aplican las normas del skill.
   */
  public static function revive(\entities\Character $character): \entities\Character
  {
    echo "Revive";
    return $character->setHealtPoints($character->getMaxHealtPoints() * 0.1);
  }


  /*
     attack: Recibe el skill a utilizar y realiza el calculo de daño a causar:
    Atáques de tipo fisicos se benefician de la fuerza del personaje: 
     -por cada 10 puntos de fuerza -> el daño aumenta en un 2%, 
     -Por cada 10 puntos de agilidad del personaje la probabilidad de un impacto crítico aumenta en un 1%, 
      si el impacto es crítico entonces el daño causado se multiplica por 150%; 
    Ataques de tipo mágico:
     -intelecto es quien incrementa el daño un 2% por cada 10 puntos.      */

  public static function attack(
    \entities\Skills\Skill $skill,
    \entities\Character $characterAffected,
    \entities\Character $characterAttacked,
    \entities\Weapon $weapon
  ) {

    if ($characterAffected->getAlive() == TRUE && $characterAttacked->getAlive() == TRUE) {
      echo $characterAttacked->getName() . " ataca con " . $skill->getName() . " a " . $characterAffected->getName() . "</br>";
      //Calculo del daño de acuerdo al arma y a la habilidad
      $damage = $weapon->getDamage() * $skill->getDamagePercentage();
      //Calculo de los daños del afectado y beneficios del atacante
      if ($skill->getType()->getId() == 1) {
        $characterAttacked->getStr() / 10 * 1.02;
        $characterAttacked->getAgi() / 10 * 1.02;
      } else {
        $characterAttacked->getIntl() / 10 * 1.02;
      }
      $characterAttacked = LevelManager::getExpForLevel($characterAttacked, EXP_ADVANCED);
      $characterAffected = self::takeDamage($damage, $characterAffected);
      \entities\GameAnnouncer::progressCharacter($characterAttacked);
      \entities\GameAnnouncer::progressCharacter($characterAffected);
    } else {
      echo "Alguno de los personajes está muerto";
    }
  }
  /*
  *takeDamage: Recibe la cantidad de daño que afectará los healtpoints del
  *personaje teniendo en cuenta que el daño recibido se reduce en 1% por cada 10
  *puntos de armadura del personaje, además la defensa aplicada en el cálculo debe
  *corresponder al tipo de ataque recibido (mágico o físico). Si el daño recibido deja
  *los healtpoints < 0, entonces el personaje debe morir.
   */
  public static function takeDamage($damage, \entities\Character $character)
  {
    $character->setHealtPoints($character->getHealtPoints() - $damage <= 0 ? 0
      : $character->getHealtPoints() - $damage);

    $character = LevelManager::getExpForLevel($character, -EXP_ADVANCED);
    $character = ($character->getHealtPoints() <= 0) ? self::die($character) : $character;


    return $character;
  }
}
