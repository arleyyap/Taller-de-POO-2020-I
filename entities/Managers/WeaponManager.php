<?php

namespace entities\Managers;

class WeaponManager
{
  public static function getWeapons(\entities\Character $character): array
  {
    $weapons = $character->getplayableClass()::getWeapon();
    return  $weapons;
  }
}
