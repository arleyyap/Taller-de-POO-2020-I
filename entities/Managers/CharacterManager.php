<?php

namespace entities\Managers;

class CharacterManager
{

    public static function create($name, $sex, $bodyType, $race, $playableClass)
    {
        try {
            self::validateCharacter($name, $sex, $bodyType, $race, $playableClass);
        } catch (Exception  $e) {
            throw new \Exception($e);
        }
        // Load initial statistics
        [$maxHealtPoints, $str, $intl, $agi, $pDef, $mDef] = $race::getStats();
        // Assign experience
        $xp = 10;
        // when the character is created, life points are the maximum he can have
        $healtPoints = $maxHealtPoints;
        // Assign level and define a character alive
        $level = 1;
        $alive = TRUE;
        // Assign initial skill to the character
        $skills = [\entities\Managers\SkillManager::loadSkillInit()];
        $character = new \entities\Character(
            $name,
            $sex,
            $bodyType,
            $race,
            $playableClass,
            $str,
            $intl,
            $agi,
            $pDef,
            $mDef,
            $xp,
            $healtPoints,
            $maxHealtPoints,
            $level,
            $skills,
            $alive
        );

        \entities\GameAnnouncer::presentCharacter($character);
        return  $character;
    }

    private function validateCharacter($name, $sex, $bodyType, $race, $playableClass)
    {
        $error = "";
        if (!$name) {
            $error = $error . "The character hasn't a name,\n";
        }

        if (!$sex) {
            $error = $error . "The character hasn't a sex,\n";
        }

        if (!$bodyType) {
            $error = $error . "The character hasn't a body type,\n";
        }

        if (!$race) {
            $error = $error . "The character hasn't a race,\n";
        }

        if (!$playableClass) {
            $error = $error . "The character hasn't a playable Class,\n";
        }

        if (strlen($error)) {
            throw new \Exception($error);
        }
    }
}
