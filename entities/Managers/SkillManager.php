<?php

namespace entities\Managers;

class SkillManager
{

    public static function loadSkillInit()
    {
        return self::gunStrike();
    }

    public static function learnSkill(\entities\Character $character, \entities\Skills\Skill $skillToLearn)
    {
        // get skills according to the character
        $enabledTypes = $character->getPlayableClass()::getTypes();
        foreach ($enabledTypes as $type) {
            // validated if skillToLearn is valid
            if ($type->getId() == $skillToLearn->getType()->getId() and $type->getSubtype()->getId() == $skillToLearn->getType()->getSubtype()->getId()) {
                $currentSkills = $character->getSkills();
                array_push($currentSkills, $skillToLearn);
                $character->setSkills($currentSkills);
                echo "The character learn the skill  " . $skillToLearn->getName() . "</br>";
                return true;
            }
        }
        echo "The character does not learn the skill " . $skillToLearn->getName() . "</br>";
        return false;
    }

    public static function forgetSkill(\entities\Character $character, \entities\Skills\Skill $skillToForget)
    {
        // get skills the character
        $skills = $character->getSkills();
        foreach ($skills as $index => $skill) {
            // validate if the skill is in the character
            if ($skill->getType()->getId() == $skillToForget->getType()->getId() and $skill->getType()->getSubtype()->getId() == $skillToForget->getType()->getSubtype()->getId()) {
                unset($skills[$index]);
                $character->setSkills($skills);
                echo "The character forget the skill " . $skillToForget->getName() . "</br>";
                return true;
            }
        }
        echo "The character does not forget the skill " . $skillToForget->getName() . "</br>";
        return false;
    }

    public static function gunStrike()
    {
        $subType = new \entities\Skills\SubType(
            1,
            "Basico",
            "Golpe de subtipo Basico"
        );
        $type = new \entities\Skills\Type(
            1,
            "Fisico",
            "Golpe de tipo Fisico",
            $subType
        );
        return new \entities\Skills\Skill(
            "Golpe con arma",
            "El personaje ataca inflingiendo el 100% del daño de arma si esta es de mano derecha o dos manos, pero de ser de mano izquierda inflingirá 70%",
            $type,
            1
        );
    }

    public static function trapperBlow()
    {
        $subTypeRogue = new \entities\Skills\SubType(2, "Picaro", "Golpe de subtipo Picaro");
        $typePhysicalRogue = new \entities\Skills\Type(1, "Fisico", "Golpe de tipo Fisico", $subTypeRogue);
        return $skill = new \entities\Skills\Skill(
            "Golpe trampero",
            "El personaje distrae a su oponente con un movimiento malintencionado asestando un golpe ",
            $typePhysicalRogue,
            1
        );
    }

    public static function deadlyPit()
    {
        $subTypeWarrior = new \entities\Skills\SubType(3, "Guerrero", "Golpe de subtipo Guerrero");
        $typePhysicalWarrior = new \entities\Skills\Type(1, "Fisico", "Golpe de tipo Fisico", $subTypeWarrior);
        return $skill = new \entities\Skills\Skill(
            "Tajo mortal",
            "El personaje salta con intenciones despiadadas y raja a su enemigo",
            $typePhysicalWarrior,
            1
        );
    }

    public static function meditation()
    {
        $subTypeBasic = new \entities\Skills\SubType(1, "Basico", "Golpe de subtipo Basico");
        $typeMagicalBasic = new \entities\Skills\Type(2, "Magico", "Golpe de tipo Magico", $subTypeBasic);
        return $skill = new \entities\Skills\Skill(
            "Meditación",
            "El personaje medita un momento",
            $typeMagicalBasic,
            1
        );
    }

    public static function calcination()
    {
        $subTypeWizard = new \entities\Skills\SubType(4, "Mago", "Golpe de subtipo Mago");
        $typeMagicalWizard = new \entities\Skills\Type(2, "Magico", "Golpe de tipo Magico", $subTypeWizard);
        return $skill = new \entities\Skills\Skill(
            "Calcinacion",
            "El personaje invoca el poder arcano y el elemento del fuego para quemar a su enemigo",
            $typeMagicalWizard,
            1
        );
    }

    public static function combatTactics()
    {
        $subTypeAdvanced = new \entities\Skills\SubType(5, "Avanzado", "Golpe de subtipo Avanzado");
        $typePhysicalWarrior = new \entities\Skills\Type(1, "Fisico", "Golpe de tipo Fisico", $subTypeAdvanced);
        return $skill = new \entities\Skills\Skill(
            "Tacticas de combate",
            "El personaje repasa el campo de batalla preparando su siguiente golpe",
            $typePhysicalWarrior,
            1
        );
    }
}
