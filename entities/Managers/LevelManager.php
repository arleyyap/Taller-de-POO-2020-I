<?php

namespace entities\Managers;

class LevelManager
{
    private static $baseExp = 100;
    private static $maxLevel = 100;
    private static $minLevel = 1;

    private function levelUp(\entities\Character $character)
    {
        //Evalúa si el nivel del personaje se encuentra por debajo del maximo nivel o si está muerto no permite subir nivel
        if ($character->getLevel() < self::$maxLevel && $character->getLevel() <> 0) {
            $character->setLevel($character->getLevel() + 1);
            echo $character->getName() . " ha ascendido a nivel " . $character->getLevel() . "!!!</br>";
            return $character;
        } else {
            echo $character->getName() . " No puede ascender " . $character->getLevel() . "!!!</br>";
            return $character;
        }
    }
    public static function levelDown(\entities\Character $character)
    {
        //Evalúa si el nivel del personaje se encuentra por debajo del minimo nivel entonces morirá, si no se le restará un nivel
        $character->setLevel($character->getLevel() - 1);
        //GameAnnouncerProgress un metodo de alerta
        echo $character->getLevel() >= self::$minLevel ?  $character->getName() . " ha bajado a nivel " . $character->getLevel() . "!!!</br>" :
            "";
    }

    /**
     * Metodo que recibe puntos de experiencia para un personake:
     * Cada que los disminuya oacumule 100 puntos,  bajará o subirá nivel.
     *dependiendo si el parametro de $exp es positivo o negativo: acumulará o disminuirá 
     *la experiencia actual del $character  
     **/
    public static function getExpForLevel(\entities\Character $character, $exp): \entities\Character
    {

        $actualExp = $character->getXp() + $exp < 0 ? 0 : $character->getXp() + $exp;
        echo $character->getName() . " con experiencia de " . $character->getXp() . " pasa a " . $actualExp . "</br>";
        //Si  actual/ base y  la anterior/base es diferente significa que puede cambiar de nivel
        if (intval($actualExp / self::$baseExp, 10) <> intval($character->getXp() / self::$baseExp, 10)) {
            if ($actualExp > $character->getXp()) {
                $character = self::levelUp($character);
                $character->setXp($actualExp);
                return $character;
            } else if ($actualExp >= 0) {
                $character = self::levelDown($character);
                $character->setXp($actualExp);
                return $character;
            }
        } else {
            $character->setXp($actualExp);
            return $character;
        }
    }
}
