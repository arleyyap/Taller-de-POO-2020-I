<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlayableClass
 *
 * @author alexa
 */

namespace entities\classes;

abstract class PlayableClass implements \interfaces\PlayableClassI {

    public static function getClassName() {
        $nameArray = explode('\\',get_called_class());
        return $nameArray[sizeof($nameArray) - 1];
    }

    public abstract function getDamage(): array;

    public abstract function getTypes(): array;

    public abstract function getWeapon(): array ;
}
