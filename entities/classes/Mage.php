<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mage
 *
 * @author alexa
 * 
 * Golpe con arma: Físico / Básico: El personaje ataca inflingiendo el 100% del daño de arma si esta es de mano derecha o dos manos, pero de ser de mano izquierda inflingirá 70% 
 *Golpe trampero : Físico / Picaro El personaje distrae a su oponente con un movimiento malintencionado asestando un golpe con arma que inflije 150% de daño con ambas armas
 *Tajo mortal: Físico / Guerrero: El personaje salta con intenciones despiadadas y raja a su enemigo inflingiendo 200% de daño con armas. 
 * Meditación: Mágico / Básico: El personaje medita un momento incrementando su agilidad e intelecto en 5%.
* Calcinación: Mágico / Mago: El personaje invoca el poder arcano y el elemento del fuego para quemar a su enemigo inflingiendo 40% de su intelecto como daño mágico. 
 * Tacticas de combate Físico / Avanzado El personaje repasa el campo de batalla preparando su siguiente golpe, esto incrementa su fuerza y agilidad en un 5%.
 * Los Magos pueden usar armas de dos manos como:bastones; también armas de una mano como: varitas, espadas y dagas.
 * Los guerreros pueden usar armas de dos manos como: espadas, bastones y hachas; también armas de una mano como: dagas, espadas y hachas.    
*  Los picaros no pueden usar armas de dos manos, pero son expertos en armas de una mano como: dagas, espadas y hachas.
 */

namespace entities\classes;

class Mage extends \entities\classes\PlayableClass {

    public function getTypes(): array {
        $subTypeBasic = new \entities\Skills\SubType(1, "Basico","Golpe de subtipo Basico");
        $subTypeWizard = new \entities\Skills\SubType(4,"Mago","Golpe de subtipo Mago");

        $typeMagicalBasic = new \entities\Skills\Type(2,"Magico", "Golpe de tipo Magico", $subTypeBasic);
        $typeMagicalWizard = new \entities\Skills\Type(2,"Magico", "Golpe de tipo Magico", $subTypeWizard);

        return [$typeMagicalBasic, $typeMagicalWizard];
    }

    public function getWeapon(): array{
        $weapon1 = new \entities\Weapon("Cane", true, true, 15);
        $weapon2 = new \entities\Weapon("Wand", false, true, 12);
        $weapon3 = new \entities\Weapon("dagger", true, true, 50);
        $weapon4 = new \entities\Weapon("Sword", false, true, 30);

        return [$weapon1,$weapon2,$weapon3,$weapon4];
    }

    public function getDamage(): array
    {
        // TODO: Implement getDamage() method.
    }
}
