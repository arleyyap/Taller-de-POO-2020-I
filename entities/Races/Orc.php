<?php

namespace entities\Races;

class Orc extends \entities\Races\Race
{
    public function getStats(): array
    {
        return [
            BASE_HEAL_POINTS * 1.05,
            BASE_STRENGTH * 1.08,
            BASE_INTELLECT * 0.95,
            BASE_AGILITY,
            BASE_PHYSICAL_DEFENSE,
            BASE_MAGIC_DEFENSE
        ];
    }
}
