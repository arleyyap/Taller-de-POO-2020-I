<?php


namespace entities\Races;


class Elf extends \entities\Races\Race
{
    public function getStats(): array
    {
        return [
            BASE_HEAL_POINTS,
            BASE_STRENGTH,
            BASE_INTELLECT * 1.10,
            BASE_AGILITY * 1.05,
            BASE_PHYSICAL_DEFENSE * 0.95,
            BASE_MAGIC_DEFENSE
        ];
    }
}
