<?php

namespace entities\Races;

class Human extends \entities\Races\Race
{
    public function getStats(): array
    {
        return [
            BASE_HEAL_POINTS,
            BASE_STRENGTH,
            BASE_INTELLECT * 1.02,
            BASE_AGILITY * 1.05,
            BASE_PHYSICAL_DEFENSE,
            BASE_MAGIC_DEFENSE
        ];
    }
}
