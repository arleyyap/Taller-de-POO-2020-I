<?php


namespace entities\Races;


class Dwarf extends \entities\Races\Race
{
    public function getStats(): array
    {
        return [
            BASE_HEAL_POINTS * 1.04,
            BASE_STRENGTH,
            BASE_INTELLECT * 1.02,
            BASE_AGILITY,
            BASE_PHYSICAL_DEFENSE,
            BASE_MAGIC_DEFENSE
        ];
    }
}
