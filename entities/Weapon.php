<?php

namespace entities;

class Weapon
{
    private $nameWeapon;
    private $weaponLeftHand;
    private $weaponRightHand;
    private $damage;


    //Los Magos pueden usar armas de dos manos como:bastones; también armas de una mano como: varitas, espadas y dagas.
    //Los guerreros pueden usar armas de dos manos como: espadas, bastones y hachas; también armas de una mano como: dagas, espadas y hachas.    
    //Los picaros no pueden usar armas de dos manos, pero son expertos en armas de una mano como: dagas, espadas y hachas.
    function __construct(string $nameWeapon, bool $weaponLeftHand, bool $weaponRightHand, int $damage)
    {
        $this->nameWeapon = $nameWeapon;
        $this->weaponLeftHand = $weaponLeftHand;
        $this->weaponRightHand = $weaponRightHand;
        $this->damage = $damage;
    }
    function getNameWeapon()
    {
        return $this->nameWeapon;
    }

    function getWeaponLeftHand()
    {
        return $this->weaponLeftHand;
    }

    function getWeaponRightHand()
    {
        return $this->weaponRightHand;
    }

    function setNameWeapon($nameWeapon): void
    {
        $this->nameWeapon = $nameWeapon;
    }

    function setWeaponLeftHand($weaponLeftHand): void
    {
        $this->weaponLeftHand = $weaponLeftHand;
    }

    function setWeaponRightHand($weaponRightHand): void
    {
        $this->weaponRightHand = $weaponRightHand;
    }
    function getDamage()
    {
        return $this->damage;
    }

    function setDamage($damage): void
    {
        $this->damage = $damage;
    }
}
