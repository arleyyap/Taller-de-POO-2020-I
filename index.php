<?php

require './config.php';
// usamos namespaces para estructurar con más orden nuestras clases
// el \ inicial nos ayuda a que la rita sea desde la raíz en lugar de tomar
// la ruta dinámica desde el punto en donde estamos importando una clase

try {
    // Create Character 
    $human = \entities\Managers\CharacterManager::create("Gerald", 1, 1, \entities\Races\Human::class, \entities\classes\Mage::class);
    //$orc = \entities\Managers\CharacterManager::create("Garrosh",1,1,\entities\Races\Orc::class,\entities\classes\Rogue::class);
    //$dwarf = \entities\Managers\CharacterManager::create("Thorin", 1, 1, \entities\Races\Dwarf::class, \entities\classes\Warrior::class);
    $elf = \entities\Managers\CharacterManager::create('Caranthir', 1, 1, \entities\Races\Elf::class, \entities\classes\Mage::class);

    // Create skill
    $meditation = \entities\Managers\SkillManager::meditation();

    // Learn Skill meditation
    $isValid = \entities\Managers\SkillManager::learnSkill($human, $meditation);
    if ($isValid) {
        // Create weapon
        $weapons = \entities\Managers\WeaponManager::getWeapons($human);

        // attack
        \entities\Managers\DamageManager::attack($human->getSkills()[0], $elf, $human, $weapons[3]);
        \entities\Managers\DamageManager::attack($human->getSkills()[0], $elf, $human, $weapons[2]);
        \entities\Managers\DamageManager::attack($human->getSkills()[0], $elf, $human, $weapons[3]);

        /*\entities\Managers\DamageManager::attack($human->getSkills()[0], $orc , $human, $weapons2[0]);
        \entities\Managers\DamageManager::attack($human->getSkills()[0], $dwarf , $human, $weapon);
        \entities\Managers\DamageManager::attack($human->getSkills()[0], $orc , $human, $weapon);
        \entities\Managers\DamageManager::attack($human->getSkills()[0], $dwarf , $human, $weapon);*/

        // Forget Skill
        $isForget = \entities\Managers\SkillManager::forgetSkill($human, $meditation);
    }
} catch (Exception  $e) {
    echo "Error creating Character: " . $e->getMessage();
}
